package com.chteuchteu.simpleselectivefocusdemo;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.widget.ImageView;

public class BlurTask extends AsyncTask<Void, Integer, Void> {
	private MainActivity activity;
	private ImageView imageView;
	private int blurAmount;
	private Bitmap source;
	private Bitmap mask;
	private int maskPosX;
	private int maskPosY;

	private Bitmap result;

	public BlurTask(MainActivity activity, ImageView imageView, int blurAmount, Bitmap source,
	                int maskPosX, int maskPosY) {
		this.activity = activity;
		this.imageView = imageView;
		this.blurAmount = blurAmount;
		this.source = source;
		this.mask = BitmapUtil.getDrawableAsBitmap(activity, R.drawable.mask);
		this.maskPosX = maskPosX;
		this.maskPosY = maskPosY;
	}

	@Override
	protected void onPreExecute() {
		activity.blurFinished = false;
	}

	@Override
	protected Void doInBackground(Void... params) {
		// Get the sharp part of the picture
		Bitmap sharp = BitmapUtil.applyMask(source, mask, maskPosX, maskPosY);

		// Blur it twice
		Bitmap blurryBackground = BitmapUtil.renderScriptBlur(source, activity, blurAmount);
		blurryBackground = BitmapUtil.renderScriptBlur(blurryBackground, activity, blurAmount);

		// Put all those together
		Canvas canvas = new Canvas(blurryBackground);
		Paint paint = new Paint();
		canvas.drawBitmap(sharp, maskPosX, maskPosY, paint);

		result = blurryBackground;

		return null;
	}

	@Override
	protected void onPostExecute(Void res) {
		imageView.setImageBitmap(result);
		activity.blurFinished = true;
	}
}
