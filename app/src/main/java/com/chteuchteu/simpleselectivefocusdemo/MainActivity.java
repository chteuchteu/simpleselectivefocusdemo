package com.chteuchteu.simpleselectivefocusdemo;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {
	public boolean blurFinished;
	private ImageView imageView;
	private SeekBar seekBar;
	private Bitmap mountains;
	private Bitmap mask;

	private int lastTouchedPositionX;
	private int lastTouchedPositionY;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		imageView = (ImageView) findViewById(R.id.imageView);
		imageView.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				lastTouchedPositionX = (int) event.getX();
				lastTouchedPositionY = (int) event.getY();

				refreshImageView();

				return true;
			}
		});

		seekBar = (SeekBar) findViewById(R.id.seekBar);
		seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (fromUser)
					refreshImageView();
			}

			@Override public void onStartTrackingTouch(SeekBar seekBar) { }
			@Override public void onStopTrackingTouch(SeekBar seekBar) { }
		});
		seekBar.setMax(25);

		blurFinished = true;
		lastTouchedPositionX = -1;
		lastTouchedPositionY = -1;

		mountains = BitmapUtil.getDrawableAsBitmap(this, R.drawable.mountains);
		mask = BitmapUtil.getDrawableAsBitmap(this, R.drawable.mask);

		Toast.makeText(this, "Touch the image to set the mask position", Toast.LENGTH_SHORT).show();
	}

	private void refreshImageView() {
		if (lastTouchedPositionX == -1 || lastTouchedPositionY == -1 || !blurFinished)
			return;

		if (seekBar.getProgress() == 0)
			seekBar.setProgress(1);

		int blurAmount = seekBar.getProgress();

		// The user thinks that he touched the center of the mask.
		// Instead, we need its upper left corner position
		lastTouchedPositionX -= mask.getWidth()/2;
		lastTouchedPositionY -= mask.getHeight()/2;

		// Get touched position on bitmap (we've got touched position on ImageView for now)
		int posX = mountains.getWidth() * lastTouchedPositionX / imageView.getWidth();
		int posY = mountains.getHeight() * lastTouchedPositionY / imageView.getHeight();

		new BlurTask(this, imageView, blurAmount, mountains, posX, posY).execute();
	}
}
