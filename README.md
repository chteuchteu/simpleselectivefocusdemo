# SimpleSelectiveFocusDemo #

This demo app shows how to produce an easy selective focus effect on Android. Warning : this code is far from being perfect and is more a proof of concept than a production-ready code. Please don't use it without knowing what you're doing.

### How does it work? ###
This simple effect is achieved using the following steps :
1. We create two copies of the bitmap : the first one will serve as the background, while the other one will be used as the sharp part of the picture. To avoid the second one to be too sharp, we'll use a prepared mask (stored in the drawables folder, /app/src/main/res/drawable/mask.png)
2. We blur the first one as much as we want
3. We apply the mask to the second bitmap
4. We create a bitmap from those two previous steps

![flow2.png](https://bitbucket.org/repo/pX579M/images/810407815-flow2.png)


### Demo app ###
This demo app shows how to integrate this code into a running application. By tapping on the ImageView *and* sliding the SeekBar, you can tell where the focus is and the amount of blur applied to the background. An AsyncTask will be run on both of these events, process the original picture, and set the result as imageDrawable of the ImageView.

### Screenshot ###

![Screenshot](https://bitbucket.org/repo/pX579M/images/1064935975-Screenshot2.png)

### Comments ###
Want to say thanks or have any comment about this project? Don't hesitate [to send me an email](mailto:chteuchteu@gmail.com)!